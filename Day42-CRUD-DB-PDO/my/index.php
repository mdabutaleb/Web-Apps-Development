<?php
include_once ("vendor/autoload.php");
include_once ('lib/applications.php');

use Example\Crud\Profile;

$profile = new Profile();
$profiles = $profile->findall();
?>
<html>
    <head>
        <title> <?php ECHO TITLE; ?></title>
    </head>
    <body>
        <table width="700px" border="1" cellpadding="10" align="center">
            <div align="center">
                <a href="form.php" align="center"> <h3>Add more data</h3> </a>
            </div>
            <tr>
                <th> SL </th>
                <th> User Name </th>
                <th> Email </th>
                <th> Date of Birth </th>
                <th> Action </th>
            </tr>
            <?php
            if (isset($profiles) && !empty($profiles)) {
                foreach ($profiles as $key => $value) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $value['id']; ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('uname', $value)) {
                                echo $value['uname'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('email', $value)) {
                                echo $value['email'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('date_of_birth', $value)) {
                                echo $value['date_of_birth'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <!--shoube be pass <?php echo $key ?> for session-->
                            <a href="src/show.php?id=<?php echo $value['id'] ?>">View ALL Info |</a>
                            <a href="src/edit.php?id=<?php echo $value['id'] ?>">  Edit</a>
                            <a href="src/delete.php?id=<?php echo $value['id'] ?>">| Delete </a>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <td colspan="5" align="center">
                <?php echo "No Available Data" ?>
                    <a href="form.php"> Click Here to add data</a>
                </td>
<?php }
?>
        </table>
    </body>
</html>