<?php
include_once ("vendor/autoload.php");
include_once ('lib/applications.php');

use Example\Crud\Profile;

$profile = new Profile();
$profile = $profile->findone($_GET['id']);
$data = $profile;
?>
<html>
    <head>
        <title> <?php ECHO TITLE; ?></title>
    </head>
    <body>
        <table width="700px" border="1" cellpadding="10" align="center">
            <div align="center">
                <a href="../index.php" align="center"> <h3>Back</h3> </a>
            </div>
            <tr>
                <th> SL </th>
                <th> User Name </th>
                <th> Email </th>
                <th> Date of Birth </th>
                <th> Action </th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                ?>
                <tr>
                    <td>
                        <?php echo $data['id']; ?>
                    </td>
                    <td>
                        <?php
                        if (array_key_exists('uname', $data)) {
                            echo $data['uname'];
                        } else {
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (array_key_exists('email', $data)) {
                            echo $data['email'];
                        } else {
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (array_key_exists('date_of_birth', $data)) {
                            echo $data['date_of_birth'];
                        } else {
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <a href="edit.php?id=<?php echo $_GET['id']; ?>">  Edit</a>
                        <a href="delete.php?id=<?php echo $_GET['id']; ?>">| Delete </a>
                    </td>
                </tr>
                <?php
            } else {
                header('location:../index.php');
            }
            ?>
        </table>
    </body>
</html>