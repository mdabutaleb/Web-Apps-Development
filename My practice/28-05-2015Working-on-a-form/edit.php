<?php
include_once 'lib/applications.php';
$data = '';
if (array_key_exists('id', $_GET))
    $data = findall($_GET['id']);
//debug($data);
?>
<html>
    <head>
        <title><?php echo PAGE_TITLE; ?></title>
    </head>
    <body>
        <form action="edited.php" method="post">
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>"
                   <fieldset>
                <legend><b>Account Details</b></legend>
                <label>Email *</label>
                <input type="email" name="email" id="email" autofocus="autofocus"
                       value="<?php
                       if (array_key_exists('email', $data) && !empty($data['email'])) {
                           echo $data['email'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>">

                <label>Password *</label>
                <input type="text" name="pw" id="pw"
                       value="<?php
                       if (array_key_exists('pw', $data) && !empty($data['pw'])) {
                           echo $data['pw'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>">
                <br/><br/>

                <br/><br/>
                <legend><b>Personal Details</b></legend>
                <label>First Name * </label>
                <input type="text" name="first_name" id="first_name"
                       value="<?php
                       if (array_key_exists('first_name', $data) && !empty($data['first_name'])) {
                           echo $data['first_name'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>">

                <label>Last Name</label>
                <input type="text" name="last_name" id="last_name"
                       value="<?php
                       if (array_key_exists('last_name', $data) && !empty($data['last_name'])) {
                           echo $data['last_name'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>">

                <label>Phone No </label>
                <input type="text" name="phone_no" id="phone_no"
                       value="<?php
                       if (array_key_exists('phone_no', $data) && !empty($data['phone_no'])) {
                           echo $data['phone_no'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>">

                <label>Street </label>
                <input type="text" name="street" id="street"
                       value="<?php
                       if (array_key_exists('street', $data) && !empty($data['street'])) {
                           echo $data['street'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>">

                <label>City</label>
                <input type="text" name="city" id="city"
                       value="<?php
                       if (array_key_exists('city', $data) && !empty($data['city'])) {
                           echo $data['city'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>"><br/><br/>

                <label>Country</label>
                <select name="country">
                    <option value="<?php echo $data['country'] ?>"><?php echo $data['country'] ?></option>
                    <option value="Bangladesh">Bangladesh</option>
                    <option value="India">India</option>
                    <option value="Pakisthan">Pakistan</option>
                    <option value="Srilanka">Srilanka</option>
                    <option value="Australia">Australia</option>
                </select>

                <label>Web site</label>
                <input type="url" name="website" placeholder="http://example.com"
                       value="<?php
                       if (array_key_exists('website', $data) && !empty($data['website'])) {
                           echo $data['website'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>"><br/>
                <br/>
                <br/><br/>


                <legend><b>Further Information</b> </legend><br/>
                <label>Gender *</label>
                <input type="radio" value="Male" name="gender" checked="checked">Male
                <input type="radio" value="Female" name="gender">Female
                <input type="radio" value="Other" name="gender">Other<br/>

                <label>Date of Birth *</label>
                <input type="date" name="data_of_birth" placeholder="Day - Month - Year"
                       value="<?php
                       if (array_key_exists('data_of_birth', $data) && !empty($data['data_of_birth'])) {
                           echo $data['data_of_birth'];
                       } else {
                           echo "Not Provide";
                       }
                       ?>">
                <br/>

                <label>Nationality</label>
                <select name="nationality">
                    <option value="<?php
                        if (array_key_exists('nationality', $data) && !empty($data['nationality'])) {
                            echo $data['nationality'];
                        } else {
                            echo "Not Provide";
                        }
                        ?>">
                            <?php
                            if (array_key_exists('nationality', $data) &&!empty($data['nationality'])) {
                            echo $data['nationality'];
                            } else {
                            echo "Not Provide";
                            }
                            ?>
                            
                    </option>
                    <option value="Bangladeshi">Bangladeshi</option>
                    <option value="Indian">Indian</option>
                    <option value="Pakisthani">Pakistani</option>
                </select><br/>
                <label>Children : </label>
                <input type="checkbox" name="children" value="Yes"
                       checked=""><br/>
                
                <label>Hobby</label>
                <input type="checkbox" name="hobby[]" value="Football">Football
                <input type="checkbox" name="hobby[]" value="Cricket">Cricket
                <input type="checkbox" name="hobby[]" value="Engineer">Engineer
                <input type="checkbox" name="hobby[]" value="Doctor">Doctor
                <br/><br/>           
                <input type="checkbox" name="accept" value="Yes Accepted">
                *I accept the terms & Condition            
                <input type="submit" value="Register">
        </form>
    </body>
</html>