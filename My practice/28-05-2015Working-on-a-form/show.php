<?php
include_once 'lib/applications.php';
$data = '';
if (array_key_exists('id', $_GET))
    $data = findall($_GET['id']);
//debug($data);
?>

<html>
    <head>
        <title>
            <?php echo PAGE_TITLE; ?>
        </title>
    </head>
    <body>
        <div align="center">
            <a href="add.php"><input type="button" value="Add New Info"></a>
            <a href="index.php"><input type="button" value="Go Back"></a>
            <a href="delete.php?id=<?php echo $_GET['id'] ?>">
                <input type="button" value="Delete">
            </a>
        </div>
        <div align="center"><H3>Account Details</H3></div>
        <?php
        if (isset($data) && !empty($data)) {
            ?>
            <table border="1" width="1000" cellpadding="2" align="center" bgcolor="whitesmoke">
                <tr bgcolor="gainsboro">
                    <th> SL </th>
                    <th> Email </th>
                    <th> Password </th>
                </tr>
                <tr>
                    <td>
                        <?php echo $_GET['id']+1 ?>
                    </td>
                    <td>
                        <?php
                        if (isset($data) && !empty($data['email'])) {
                            echo $data['email'];
                        } else {
                            echo "Not Provide";
                        }
                        ?>  
                    </td>
                    <td>
                        <?php
                        if (isset($data) && !empty($data['pw'])) {
                            echo $data['pw'];
                        } else {
                            echo "Not Provide";
                        }
                        ?>  
                    </td>
                </tr>
            </table><br/><br/>
            <div align="center"><H3>Personal Details</H3></div>
            <table border="1" width="1000" cellpadding="2" align="center" bgcolor="whitesmoke">
                <tr bgcolor="gainsboro">
                    <th > First Name </th>
                    <th> Last Name </th>
                    <th> Phone No </th>
                    <th> Street </th>
                    <th> City </th>
                    <th> Country </th>
                    <th> Website </th>
                </tr>
                <tr>
                    <td>
                        <?php 
                        if(array_key_exists('first_name', $data) && !empty($data['first_name'])){
                            echo $data['first_name'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('last_name', $data) && !empty($data['last_name'])){
                            echo $data['last_name'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('phone_no', $data) && !empty($data['phone_no'])){
                            echo $data['phone_no'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('street', $data) && !empty($data['street'])){
                            echo $data['street'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('city', $data) && !empty($data['city'])){
                            echo $data['city'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('country', $data) && !empty($data['country'])){
                            echo $data['country'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (array_key_exists('website', $data) && !empty($data['website'])) {
                            ?>
                            <a href="<?php echo $data['website']; ?>" target="_blank">
                                <?php echo $data['website']; ?>
                            </a>
                            <?PHp
                        } else {
                            echo "Not Provide";
                        }
                        ?>
                    </td>
                </tr>
            </table><br/><br/>
            <div align="center">
                <h3>Further Information</h3>
            </div>
            <table border="1" width="1000" cellpadding="2" align="center" bgcolor="whitesmoke">
                <tr bgcolor="gainsboro">
                    <th> Gender</th>
                    <th> Birth Date</th>
                    <th> Nationality</th>
                    <th> Children</th>
                    <th> Hobby</th>
                </tr>            
                <tr>
                   <td>
                        <?php 
                        if(array_key_exists('gender', $data) && !empty($data['gender'])){
                            echo $data['gender'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('data_of_birth', $data) && !empty($data['data_of_birth'])){
                            echo $data['data_of_birth'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('nationality', $data) && !empty($data['nationality'])){
                            echo $data['nationality'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('children', $data) && !empty($data['children'])){
                            echo $data['children'];
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                        if(array_key_exists('hobby', $data) && !empty($data['hobby'])){
                            foreach ($data['hobby'] as $select){
                                echo  " ✓  " . $select;
                            }
                        }
                        else{
                            echo "Not provide";
                        }
                        ?>
                    </td>
                </tr>
                <?php
            } else {
                header('location:index.php');
            }
            ?>
        </table>
    </body>
</html>
