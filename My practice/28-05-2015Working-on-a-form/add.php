<?php
include_once 'lib/applications.php';
?>
<html>
    <head>
        <title>
            <?php echo PAGE_TITLE; ?>
        </title>
    </head>
    <body>
        <form action="process.php" method="post">
            <fieldset>
                <legend><b>Account Details</b></legend>
                <label>Email *</label>
                <input type="email" name="email" id="email" autofocus="autofocus">
                <label>&nbsp;&nbsp;&nbsp; Repeat Email *</label>
                <input type="text" name="remail" id="remail"><br/><br/>

                <label>Password *</label>
                <input type="password" name="pw" id="pw">

                <label>&nbsp;&nbsp;&nbsp; Repeat Password *</label>
                <input type="password" name="repeat_pw" id="pw">
            </fieldset><br/><br/>
            <fieldset>
                <br/><br/>
                <legend><b>Personal Details</b></legend>
                <label>First Name * </label>
                <input type="text" name="first_name" id="first_name">

                <label>Last Name</label>
                <input type="text" name="last_name" id="last_name">

                <label>Phone No </label>
                <input type="text" name="phone_no" id="phone_no">

                <label>Street </label>
                <input type="text" name="street" id="street">

                <label>City</label>
                <input type="text" name="city" id="city"><br/><br/>

                <label>Country</label>
                <select name="country">
                    <option>Select Country</option>
                    <option value="Bangladesh">Bangladesh</option>
                    <option value="India">India</option>
                    <option value="Pakisthan">Pakistan</option>
                    <option value="Srilanka">Srilanka</option>
                    <option value="Australia">Australia</option>
                </select>

                <label>Website</label>
                <input type="url" name="website" placeholder="http://example.com"><br/>
                <br/>
            </fieldset><br/><br/>

            <fieldset>
                <legend><b>Further Information</b> </legend><br/>
                <label>Gender *</label>
                <input type="radio" value="Male" name="gender" checked="checked">Male
                <input type="radio" value="Female" name="gender">Female
                <input type="radio" value="Other" name="gender">Other<br/>

                <label>Date of Birth *</label>
                <input type="date" name="data_of_birth" placeholder="Day - Month - Year"><br/>

                <label>Nationality</label>
                <select name="nationality">
                    <option>Nationality</option>
                    <option value="Bangladeshi">Bangladeshi</option>
                    <option value="Indian">Indian</option>
                    <option value="Pakisthani">Pakistani</option>
                </select><br/>
                <label>Children : </label>
                <input type="checkbox" name="children" value="Yes"><br/>
                <label>Hobby</label>
                <input type="checkbox" name="hobby[]" value="Football">Football
                <input type="checkbox" name="hobby[]" value="Cricket">Cricket
                <input type="checkbox" name="hobby[]" value="Engineer">Engineer
                <input type="checkbox" name="hobby[]" value="Doctor">Doctor
            </fieldset>
            <br/><br/>           
            <input type="checkbox" name="accept" value="Yes Accepted">
                *I accept the terms & Condition            
            <input type="submit" value="Register">
        </form>
    </body>
</html>