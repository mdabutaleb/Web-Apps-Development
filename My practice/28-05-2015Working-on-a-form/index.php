<?php
include_once 'lib/applications.php';
$data = '';
if (array_key_exists('formdata', $_SESSION)) {
    $data = $_SESSION['formdata'];
}
?>
<html>
    <head>
        <title>
            <?php echo PAGE_TITLE; ?>
        </title>
    </head>
    <body>
        <div align="center">
            <a href="add.php"><h3>Add New Info</h3></a>
        </div>
        <table border="1" cellpadding="2" align="center" bgcolor="whitesmoke">
            <tr bgcolor="gainsboro">
                <th>SL</th>
                <th> Email </th>
                <th> Password </th>
                <th> First Name </th>
                <th> Last Name </th>
                <th> Phone No </th>
                <th> Street </th>
                <th> City </th>
                <th> Country </th>
                <th> Website </th>
<!--                <th> Gender</th>
                <th> Birth Date</th>
                <th> Nationality</th>
                <th> Children</th>
                <th> Hobby</th>-->
                <th colspan="3"> Action</th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $key => $value) {
                    ?>
                    <tr>
                        <td> <?php echo $key + 1 ?></td>
                        <td>
                            <?php
                            if (array_key_exists('email', $value) && !empty($value['email'])) {
                                echo $value['email'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('pw', $value) && !empty($value['pw'])) {
                                echo $value['pw'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('first_name', $value) && !empty($value['first_name'])) {
                                echo $value['first_name'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('last_name', $value) && !empty($value['last_name'])) {
                                echo $value['last_name'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('phone_no', $value) && !empty($value['phone_no'])) {
                                echo $value['phone_no'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('street', $value) && !empty($value['street'])) {
                                echo $value['street'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('city', $value) && !empty($value['city'])) {
                                echo $value['city'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('country', $value) && !empty($value['country'])) {
                                echo $value['country'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('website', $value) && !empty($value['website'])) {
                                ?>
                                <a href="<?php echo $value['website']; ?>" target="_blank">
                                    <?php echo $value['website']; ?>
                                </a>
                                <?PHp
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
<!--                        <td>
                            <?php
                            if (array_key_exists('gender', $value) && !empty($value['gender'])) {
                                echo $value['gender'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('data_of_birth', $value) && !empty($value['data_of_birth'])) {
                                echo $value['data_of_birth'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('nationality', $value) && !empty($value['nationality'])) {
                                echo $value['nationality'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('children', $value) && !empty($value['children'])) {
                                echo $value['children'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>


                        <td>
                            <?php
                            if (array_key_exists('hobby', $value)) {
//                                debug($value['hobby']);
                                foreach ($value['hobby'] as $onedata)
                                    echo " ✓ " . $onedata . " <br/> ";
                            }
                            ?>
                        </td>-->
                        <td>
                            <a href="show.php?id=<?php echo $key ?>">Full info</a>
                        </td>
                        <td>
                            <a href="edit.php?id=<?php echo $key ?>">Edit</a>
                        </td>
                        <td>
                            <a href="delete.php?id=<?php echo $key ?>">Delete</a>
                        </td>
                    </tr>
                    <?php
                }
            } else { ?>
                <tr>
                    <td colspan="16" align="center">
              <?php   echo "No Available Data In the table"; ?>
                </td>
                </tr>
          <?php  }
            ?>
        </table>
    </body>
</html>
