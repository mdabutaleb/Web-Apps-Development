<?php

class sum {

    public $a = '';
    public $b = '';
    public $c = '';

    public function add($g = '', $k = '') {
        $this->a = $g;
        $this->b = $k;
        $this->c = $this->a + $this->b;
        echo $this->c;
    }

}

$newobject = new sum();
$newobject->add(20, 30);
echo "</br>Calling a property</br>";
echo $newobject->a;
//Sub class of sum class
class subofsum extends sum {
    
}

$newsubofsum = new subofsum();
echo "<br/>Output of 2nd class (sub clas)<br/>---------------------<br/>";
$newsubofsum->add(10, 1);

//new class 
class sum2 {


    public function myfunction($a='', $b='') {
        $newobject = new sum();
        $newobject->add($a, $b);
    }

}

echo "<br/>Out of another class</br>";

$sum2 = new sum2();
echo $sum2->myfunction(40,40);
