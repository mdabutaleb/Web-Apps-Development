<?php
include_once('lib/application.php');

$query = "SELECT * FROM tbl_gender";
$result = mysqli_query($link, $query);
//$sl = 0;
$data = array();
while ($row = mysqli_fetch_assoc($result)) {
    $data[] = $row;
}
?>
<html>
    <head>
        <meta charset = "UTF-8">
        <title><?php echo PAGE_TITLE; ?></title>
    </head>
    <body>
        <div>
            <h1 style="color:green;">
                <?php
                if (isset($_SESSION['message1']) && !empty($_SESSION['message1'])) {
                    echo $_SESSION['message1'];
                    unset($_SESSION['message1']);
                }
                ?>
            </h1>
            <h1 style="color:red;">
                <?php
                if (isset($_SESSION['message2']) && !empty($_SESSION['message2'])) {
                    echo $_SESSION['message2'];
                    unset($_SESSION['message2']);
                }
                ?>
            </h1>
            <!-- Use Proper table format-->
            <table border="1" cellspacing="1" cellpadding="20">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Gender</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($data) > 0) {
                        foreach ($data as $value) {
                            ?> 
                            <tr>
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $value['full_name']; ?></td>
                                <td><?php echo $value['gender']; ?></td>
                                <td>
                                    <a href="show.php?id=<?php echo $value['id']; ?>">Show</a>
                                    |<a href="edit.php?id=<?php echo $value['id']; ?>">Edit</a>
                                    |<a href="delete.php?id=<?php echo $value['id']; ?>">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="5" align="center">
                                <?php echo "No data available"; ?>
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
        <nav>
            <li><a href="create.php">Add</a></li>
        </nav>
    </body>
</html>


