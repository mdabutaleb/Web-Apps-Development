<?php
include_once('lib/application.php');
?>
<html>
    <head>
        <title><?php echo PAGE_TITLE; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <form action="add.php" method="post">
                <fieldset>
                    <legend>City Information</legend>
                    <div>
                        <label for="txtFullName">Full Name:</label>
                        <input type="text" name="full_name" id="txtFullName" value=""/>
                    </div> 
                    <div>
                        <label>City:</label>
                        <select name="city" id="optCity"> 
                            <option>Please Select City......</option>
                            <option value="Dhaka">Dhaka</option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Comilla">Comilla</option>
                            <option value="Chittagang">Chittagong</option>
                            <option value="Pabna">Pabna</option>
                            <option value="Barishal">Barishal</option>
                        </select>
                    </div>
                    <input type="Submit" value="Save Information" />
                </fieldset>    
            </form>
        </div>
        <nav>
            <li><a href="index.php">List</a></li>
        </nav>
    </body>
</html>















