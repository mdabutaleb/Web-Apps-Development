<?php
include_once('lib/application.php');

$id = $_GET['id'];

$query = "SELECT * FROM tbl_city WHERE id=" . $id;
$result = mysqli_query($link, $query);
$row = mysqli_fetch_assoc($result);
$data = $row;

//var_dump($data);

$optDhaka = "";
$optRajshahi = "";
$optComilla = "";
$optChittagong = "";
$optPabna = "";
$optBarishal = "";


if ($data['city'] == 'Dhaka') {
    $optDhaka = "selected='selected'";
}
if ($data['city'] == 'Rajshahi') {
    $optRajshahi = "selected='selected'";
}
if ($data['city'] == 'Comilla') {
    $optComilla = "selected='selected'";
}
if ($data['city'] == 'Chittagong') {
    $optChittagong = "selected='selected'";
}
if ($data['city'] == 'Pabna') {
    $optPabna = "selected='selected'";
}
if ($data['city'] == 'Barishal') {
    $optBarishal = "selected='selected'";
}
?>

<html>
    <head>
        <title><?php echo PAGE_TITLE; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <form action="store.php?id=<?php echo $id; ?>" method="post">
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <fieldset>
                    <legend>City Information</legend>
                    <div>
                        <label for="txtFullName">Full Name:</label>
                        <input type="text" name="full_name" id="txtFullName" value="<?php echo $data['full_name'] ?>" />
                    </div>
                    <div>
                        <label>City:</label>
                        <select name="city" id="optCity"> 
                            <option>Please Select City......</option>
                            <option value="Dhaka" <?php echo $optDhaka; ?>>Dhaka</option>
                            <option value="Rajshahi" <?php echo $optRajshahi; ?>>Rajshahi</option>
                            <option value="Comilla" <?php echo $optComilla; ?>>Comilla</option>
                            <option value="Chittagang" <?php echo $optChittagong; ?>>Chittagong</option>
                            <option value="Pabna" <?php echo $optPabna; ?>>Pabna</option>
                            <option value="Barishal" <?php echo $optBarishal; ?>>Barishal</option>
                        </select>
                    </div>
                    <input type="Submit" value="Save Information"/>
                </fieldset>    
            </form>
        </div>
        <nav>
            <li><a href="create.php">Add</a></li>
            <li><a href="index.php">List</a></li>
            <li><a href="show.php?id=<?php echo $id; ?>">Show</a></li>
            <li><a href="delete.php?id=<?php echo $id; ?>">Delete</a></li>
        </nav>   
    </body>
</html>


