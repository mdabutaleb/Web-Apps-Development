<?php
include_once('lib/application.php');

$id = $_GET['id'];

$query = "SELECT * FROM tbl_city WHERE id=" . $id;
$result = mysqli_query($link, $query);
$row = mysqli_fetch_assoc($result);
$data = $row;
?>
<html>
    <head>
        <meta charset = "UTF-8">
        <title><?php echo PAGE_TITLE; ?></title>
    </head>
    <body>
        <div>
            <fieldset>
                <legend>City Details</legend>
                <dl>
                    <dt>ID</dt>
                    <dd><?php echo $data['id']; ?></dd>
                    <dt>Full Name</dt>
                    <dd><?php echo $data['full_name']; ?></dd>
                    <dt>City</dt>
                    <dd><?php echo $data['city']; ?></dd>
                </dl>
            </fieldset>
        </div>
        <nav>
            <li><a href="create.php">Add</a></li>
            <li><a href="index.php">List</a></li>
            <li><a href="edit.php?id=<?php echo $id; ?>">Edit</a></li>
            <li><a href="delete.php?id=<?php echo $id; ?>">Delete</a></li>
        </nav> 
    </body>
</html>

