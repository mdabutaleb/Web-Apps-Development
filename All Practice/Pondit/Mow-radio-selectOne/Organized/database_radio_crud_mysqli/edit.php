<?php
include_once('lib/application.php');

$id = $_GET['id'];

$query = "SELECT * FROM tbl_gender WHERE id= " . $id;
$result = mysqli_query($link, $query);
$row = mysqli_fetch_assoc($result);
$data = $row;
?>
<html>
    <head>
        <title><?php echo PAGE_TITLE; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <form action="store.php?id=<?php echo $id; ?>" method="post">
                <!--<input type="hidden" name="id" value="<?php echo $id; ?>" />-->
                <fieldset>
                    <legend>Gender Information</legend>
                    <div>
                        <label for="txtFullName">Full Name:</label>
                        <input type="text" name="full_name" id="txtFullName" value="<?php echo $data['full_name'] ?>" />
                    </div>
                    <div>
                        <label>Gender:</label>
                        <?php
                        if ($data['gender'] == 'male') {
                            ?>
                            <input type="radio" name="gender" id="optGender1" value="male" checked="checked" />
                            <label for="optGender1">Male</label>

                            <?php
                        } else {
                            ?>
                            <input type="radio" name="gender" id="optGender1" value="male" />
                            <label for="optGender1">Male</label>
                            <?php
                        }
                        if ($data['gender'] == 'female') {
                            ?>
                            <input type="radio" name="gender" id="optGender2" value="female" checked="checked"  />
                            <label for="optGender2">Female</label>
                            <?php
                        } else {
                            ?>
                            <input type="radio" name="gender" id="optGender2" value="female" />
                            <label for="optGender2">Female</label>
                        <?php } ?>
                    </div>
                    <input type="Submit" value="Save Information"/>
                </fieldset>
            </form>
        </div>
        <nav>
            <li><a href="create.php">Add</a></li>
            <li><a href="index.php">List</a></li>
            <li><a href="show.php?id=<?php echo $id; ?>">Show</a></li>
            <li><a href="delete.php?id=<?php echo $id; ?>">Delete</a></li>
        </nav>
    </body>
</html>


