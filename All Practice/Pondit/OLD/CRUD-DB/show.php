<?php
include_once 'src/apps.php';
$data = findone($_GET['id']);
//debug($data);
?>

<fieldset> 
    <legend>  Understanding CRUD BY Database using Textarea </legend><br/>
    <table border="1" width="" align="center" cellpadding="20">
        <div align="center">
            <a href="form.php"><button>Add new Message</button></a> 
            <a href="index.php"><button>Back To the list</button></a>
        </div><br/>
        <tr>
            <th>SL</th>
            <th>Message</th>
            <th>Action</th>
        </tr>
        <?php if (isset($data) && !empty($data)) { ?>
            <tr>
                <td> <?php echo $_GET['id'] ?></td>
                <td>
                    <?php
                    if (array_key_exists('textmessage', $data)) {
                        echo $data['textmessage'];
                    } else {
                        echo "No message here";
                    }
                    ?>
                </td>
                <td> 
                    <a href="edit.php?id=<?php echo $_GET['id'] ?>"><button>Edit</button></a>
                    <a href="delete.php?id=<?php echo $_GET['id'] ?>"><button>Delete</button></a> 
                </td>
            </tr>
            <?php
        } else {
            header('location:index.php');
        }
        ?>
    </table><br/><Br/><br/>


</fieldset>


