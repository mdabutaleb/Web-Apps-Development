<?php
include_once 'src/apps.php';
$data = findall();
//debug($data);
?>

<table border="1" width="" align="center" cellpadding="20">
    <div align="center">
        <a href="form.php"><button>Add new Message</button></a> 
    </div><br/>
    <tr>
        <th>SL</th>
        <th>Message</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($data) && !empty($data)){
        foreach ($data as $value) { {
                ?>
                <tr>
                    <td><?php echo $value['id'] ?></td>
                    <td>
                        <?php
                        if (array_key_exists('textmessage', $value)) {
                            echo $value['textmessage'];
                        } else {
                            echo "No message here";
                        }
                        ?>
                    </td>
                    <td>
                        <a href="show.php?id=<?php echo $value['id']  ?>"><button> Show </button></a>  
                        <a href="edit.php?id=<?php echo $value['id']  ?>"> <button> Edit </button></a> 
                        <a href="delete.php?id=<?php echo $value['id']  ?>"> <button>Delete</button></a> 
                    </td>
                </tr>
                <?php
            }
        }
    } else {
        ?>
        <tr>
            <td colspan="3" align="center">
                <?php echo "No data available"; ?>
            </td>
        </tr>
    <?php }
    ?>
</table>