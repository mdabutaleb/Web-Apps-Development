<?php
include_once 'src/apps.php';
$data = findone($_GET['id']);
//debug($data) & DIE();
?>

<fieldset> 
    <legend>  Understanding CRUD BY Database using Textarea </legend><br/><Br/>
    <form method="POST" action="edit_process.php">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" />
        <table border="1" width="" align="center" cellpadding="20">
            <div align="center">
                <a href="form.php"><button>Add new Message</button></a> 
                <a href="index.php"><button>Back To the list</button></a>
            </div><br/>
            <tr>
                <th>SL</th>
                <th>Message</th>
                <th>Action</th>
            </tr>
            <?php if (isset($data) && !empty($data)) { ?>
                <tr>
                    <td> <?php echo $_GET['id'] ?></td>
                    <td>
                        <textarea rows="4" cols="50" name="textmessage"><?php
                            if (array_key_exists('textmessage', $data)) {
                                echo $data['textmessage'];
                            } else {
                                echo "No message here";
                            }
                            ?>
                        </textarea>
                    </td>
                    <td>
                        <input type="submit" value="Update" />
                        <button><a href="delete.php?id=<?php echo $_GET['id'] ?>">Delete </a></button>
                    </td>
                </tr>
                <?php
            } else {
                header('location:index.php');
            }
            ?>
        </table><br/><br/><br/>
    </form>

</fieldset>


