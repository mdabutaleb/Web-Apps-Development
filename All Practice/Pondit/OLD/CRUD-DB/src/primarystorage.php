<?php

//Data Storing in the session
function create() {
    if ($_SERVER['REQUEST_METHOD'] = "POST") {
        if (!array_key_exists('formdata', $_SESSION)) {
            $_SESSION['formdata'] = array();
        }
        if (array_key_exists('textmessage', $_POST) && !empty($_POST['textmessage'])) {
            $_SESSION['formdata'][] = $_POST;
        }
        header('location:index.php');
    }
}

// Getting all data from the session
function findall() {
    if (array_key_exists('formdata', $_SESSION)) {
        return $_SESSION['formdata'];
    }
}

// Getting one value from the session
function findone() {
    if (array_key_exists('formdata', $_SESSION)) {
        return $_SESSION['formdata'][$_GET['id']];
    }
}

// Updating Date

function update($id = '') {
    if (array_key_exists('textmessage', $_POST) && !empty($_POST['textmessage'])) {
        $_SESSION['formdata'][$id] = $_POST;
    }
    header('location:index.php');
}


//Deleting one data

function delete($id=''){
    if(array_key_exists('formdata', $_SESSION)){
        unset($_SESSION['formdata'][$id]);
    }
    header('location:index.php');
}