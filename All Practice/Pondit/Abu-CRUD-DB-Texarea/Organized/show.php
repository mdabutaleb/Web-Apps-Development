<?php
include_once 'lib/apps.php';
$data = findone();
?>

<html>
    <head>
        <title>Understanding CRUD BY DB using Textarea</title>
    </head>
    <body>
        <fieldset> 
            <legend>  Understanding CRUD BY Database using Textarea </legend>

            <dl>
                <dt>Title</dt>
                <dd>
                    <?php
                    if (array_key_exists('description', $data) && !empty($data['description'])) {
                        echo $data ['description'];
                    } else {
                        echo "No Description available";
                    }
                    ?>
                </dd>
            </dl>

        </fieldset>
        <nav>
            <li><a href="create.php">Add New</a></li>
            <li><a href="index.php">List</a></li>
            <li><a href="edit.php?id=<?php echo $_GET['id'] ?>">Edit </a></li>
            <li><a href="delete.php?id=<?php echo $_GET['id'] ?>">Delete</a> </li>
        </nav>
    </body>
</html>