<?php
include_once 'lib/apps.php';
$data = findall();
?>
<html>
    <head>
        <title>Understanding CRUD BY Database using Textarea</title>
    </head>
    <body>


        <table border="1" width="" align="center" cellpadding="20">
            <div align="center">
                <a href="create.php">Create</a> 
            </div><br/>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td><?php echo $value['id'] ?></td>
                        <td>
                            <?php
                            if (array_key_exists('description', $value)) {
                                echo $value['description'];
                            } else {
                                echo "No Description available";
                            }
                            ?>
                        </td>
                        <td>
                            <a href="show.php?id=<?php echo $value['id']; ?>"> Show </a>  |
                            <a href="edit.php?id=<?php echo $value['id']; ?>"> Edit </a> |
                            <a href="delete.php?id=<?php echo $value['id']; ?>"> Delete</a> 
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="3" align="center">
                        <?php echo "No data available"; ?>
                    </td>
                </tr>
            <?php }
            ?>
        </table>    
    </body>
</html>