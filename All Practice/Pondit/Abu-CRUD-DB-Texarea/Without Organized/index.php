<?php
$conn = mysql_connect('localhost', 'root', '') or die('MySql can not connect');
mysql_select_db('crud') or die('Database can not connect');

$query = "SELECT * FROM `textarea`";
$result = mysql_query($query);
$data = array();
while ($row = mysql_fetch_assoc($result)) {
    $data[] = $row;
}
?>
<html>
    <head>
        <title>Understanding CRUD BY Database using Textarea</title>
    </head>
    <body>


        <table border="1" width="" align="center" cellpadding="20">
            <div align="center">
                <a href="create.php">Create</a> 
            </div><br/>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td><?php echo $value['id'] ?></td>
                        <td>
                            <?php
                            if (array_key_exists('description', $value)) {
                                echo $value['description'];
                            } else {
                                echo "No Description available";
                            }
                            ?>
                        </td>
                        <td>
                            <a href="show.php?id=<?php echo $value['id']; ?>"> Show </a>  |
                            <a href="edit.php?id=<?php echo $value['id']; ?>"> Edit </a> |
                            <a href="delete.php?id=<?php echo $value['id']; ?>"> Delete</a> 
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="3" align="center">
                        <?php echo "No data available"; ?>
                    </td>
                </tr>
            <?php }
            ?>
        </table>    
    </body>
</html>