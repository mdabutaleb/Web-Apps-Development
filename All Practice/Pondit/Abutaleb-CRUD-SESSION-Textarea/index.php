<?php
session_start();
if(array_key_exists('formdata', $_SESSION)){
$data = $_SESSION['formdata'];
}
?>

<table border="1" width="" align="center" cellpadding="20">
    <div align="center">
        <a href="create.php">Create</a> 
    </div><br/>
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($data) && !empty($data)){
        foreach ($data as $key=> $value) { {
                ?>
                <tr>
                    <td><?php echo $key+1 ?></td>
                    <td>
                        <?php
                        if (array_key_exists('description', $value)) {
                            echo $value['description'];
                        } else {
                            echo "No Description available";
                        }
                        ?>
                    </td>
                    <td>
                        <a href="show.php?id=<?php echo $key; ?>"> Show </a>  |
                        <a href="edit.php?id=<?php echo $key; ?>"> Edit </a> |
                        <a href="delete.php?id=<?php echo $key; ?>"> Delete</a> 
                    </td>
                </tr>
                <?php
            } 
        }
    } else {
        ?>
        <tr>
            <td colspan="3" align="center">
                <?php echo "No data available"; ?>
            </td>
        </tr>
    <?php }
    ?>
</table>