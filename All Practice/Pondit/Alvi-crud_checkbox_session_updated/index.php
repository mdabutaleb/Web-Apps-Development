<?php
include_once 'lib/app.php';

    $Hobbies = '';
    if (array_key_exists('hobbies_checkbox', $_SESSION) && !empty($_SESSION['hobbies_checkbox'])){
        $Hobbies = $_SESSION['hobbies_checkbox'];
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> The item from Checkbox Selection </title>
    </head>
    <body>
        <section>
            <div>
                <nav>
                    <ul> 
                        <li style="text-align:right; margin-right:20px;"> <a href="create.php"> Create </a>  New Hobbies </li>
                    </ul>
                </nav>
            </div>
           
            <table border="1" align="center" cellpadding="20">
                <tr>
                    <th> Serial No. </th>
                    <th> Name </th>
                    <th> Choice1 </th>
                    <th> Choice2 </th>
                    <th> Choice3 </th>
                    <th> Choice4 </th>
                    <th> Choice5 </th>
                    <th> Actions</th>
                
                </tr>
                    <?php 
                        if (isset($Hobbies) && !empty($Hobbies)){
                        foreach($Hobbies as $key=>$value) { 
                    ?>
                <tr>
                    <td> <?php echo $key+1 ?> </td>
                    <td> 
                        <?php 
                            if(array_key_exists('fullName',$value) && !empty ($value['fullName'])){
                                
                                print $value['fullName']; 
                            }   else{
                                    echo " Not Provided";
                                }
                        ?> 
                    </td>
                    <td> 
                        <?php 
                            if(array_key_exists('boat',$value) && !empty ($value['boat'])){
                              echo $value['boat'];   
                            }   else{
                                    echo " Not Chosen";
                                }
                        ?> 
                    </td>
                    <td> 
                        <?php 
                            if(array_key_exists('code',$value) && !empty ($value['code'])){
                              echo $value['code'];   
                            }   else{
                                    echo " Not Chosen";
                                }
                        ?> 
                    </td>
                    <td> 
                        <?php 
                            if(array_key_exists('pray',$value) && !empty ($value['pray'])){
                              echo $value['pray'];   
                            }   else{
                                    echo " Not Chosen";
                                }
                        ?> 
                    </td>
                    <td> 
                        <?php 
                            if(array_key_exists('garden',$value) && !empty ($value['garden'])){
                              echo $value['garden'];   
                            }   else{
                                    echo " Not Chosen";
                                }
                        ?> 
                    </td>
                
                    <td> 
                        <?php 
                            if(array_key_exists('gym',$value) && !empty ($value['gym'])){
                              echo $value['gym'];   
                            }   else{
                                    echo " Not Chosen";
                                }
                        ?> 
                    </td>
                    <td>    
                        <a href="show.php?id=<?php echo $key; ?>"> Show </a>
                        <a href="edit.php?id=<?php echo $key; ?>"> Edit </a>
                        <a href="delete.php?id=<?php echo $key; ?>"> Delete </a> 
                    </td>
                </tr>
                <?php 
                    } 
                        }else{
                ?>    
                <tr>
                    <td colspan="8" align="center"> <?php echo"No data is available"; ?> </td>
                </tr>
                <?php } ?>
            </table>
        </section>
    </body>
</html>
