<?php
include_once 'lib/app.php';

if (array_key_exists('id', $_GET)) {
    $Hobbies = findone($_GET['id']);
}

$optBoat = '';
$optCoding = '';
$optPraying = '';
$optGardening = '';
$optGyming = '';

if (isset($Hobbies) && !empty($Hobbies)){
    if(isset($Hobbies['boat']) == 'boat'){
        $optBoat = "checked='checked'";   
    }
        
    if(isset($Hobbies['code']) == 'coding'){
        $optCoding = "checked='checked'";
    }
        
    if(isset($Hobbies['pray']) == 'praying'){
        $optPraying = "checked='checked'";   
    }
        
    if(isset($Hobbies['garden'])== 'gardening'){
        $optGardening = "checked='checked'";
    }
        
    if(isset($Hobbies['gym'])== 'gyming'){
        $optGyming = "checked='checked'";
    }
}


debug($Hobbies);
?>

<html>
    <head>
        <title> Showing selected Hobbies for editing</title>
    </head>
    <body>
        <nav>
            <ul> 
                <li> <a href="index.php"> List </a> </li>
                <li><a href="create.php"> Add </a> </li>
                <li> <a href="delete.php?id=<?php echo $_GET['id'];?>"> Delete </a></li>
            </ul>
        </nav>
        
        <form action="store.php" method="POST"> 
            <input type="hidden" name="id" value="<?php echo $_GET ['id'];?>">
            <label for="1"> Enter Your Name</label>
                    <input type="text" name="fullName" id="1" value="<?php echo $Hobbies['fullName'];?>" />
                     <fieldset>
                         
			<legend> Edit Your Hobby </legend>
                        
                        <p><input type="checkbox" name="boat" value="boat" <?php echo $optBoat;?>/> I enjoy boat journey </p>
		
			<p><input type="checkbox" name="code" value="coding" <?php echo $optCoding;?> > I like to write codes </p>
                        <p><input type="checkbox" name="pray" value="praying" <?php echo $optPraying; ?> > I like to pray my prayers </p>
                        <p><input type="checkbox" name="garden" value="gardening" <?php echo $optGardening;?> > I like to make gardening </p>
			<p><input type="checkbox" name="gym" value="gyming" <?php echo $optGyming;?> > I like to gym regularly </p>
                        <p><input type="submit" value="submit"/>
                        <input type="reset" name="reset" value="Reset" /></p>
                    </fieldset>
            
        </form>
    </body>
</html>