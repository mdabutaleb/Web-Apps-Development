<?php
require_once './connection.php';
$sql = "SELECT * FROM file";
$result = mysql_query($sql);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>slider</title>
        <link rel="stylesheet" href="css/blindify.min.css" media="all" />
        <link rel="stylesheet" href="css/demo.css" media="all" />

    </head>
    <body>
        <article class="container">
            <h3 style="text-align: center;color: #cccccc">DYNAMIC SLIDER <a href="index.php">SHOW</a> <a href="create.php">ADD</a> <a href="gallery.php">GALLERY</a></h3>
            <hr/>
            <section id="blindify" >
                <ul>
                    <?php
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <li> <img src="<?php echo $row['img_path'] ?>" alt="image" ></li>
                    <?php } ?>
                </ul>
            </section>
        </article>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="js/jquery.blindify.min.js"></script>

    </body>
</html>