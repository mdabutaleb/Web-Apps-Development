<!DOCTYPE html>
<html>
    <head>
        <title>gallery</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <style>
            .img_size{
                width: 250px;
                height: 250px;
            }
        </style>
    </head>

    <body>
        <?php
        require_once './connection.php';
        $sql = "SELECT * FROM file";
        $result = mysql_query($sql);
        ?>
        <a href="create.php">ADD</a>
        <a href="index.php">SHOW</a>
          <a href="slider.php">SLIDER</a>
        <div class="container">
            <h2 style="text-align: center">IMAGE GALLERY</h2>
            <div class="row">
                <?php
                while ($row = mysql_fetch_array($result)) {
                    ?>
                    <div class=" col-xs-6 col-md-3">
                        <a href="#" class="img-thumbnail img_size" >
                            <img src="<?php echo $row['img_path'] ?>" alt="image" width="100%" height="100%">
                        </a>
                    </div>
                <?php } ?>
            </div>

        </div>
           
        <!-- Latest compiled and minified JavaScript -->
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

    </body>
</html>