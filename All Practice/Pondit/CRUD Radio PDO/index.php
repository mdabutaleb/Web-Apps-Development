<?php
$alldata = '';
$data = '';
try {
    $pdo = new PDO('mysql:host=localhost;dbname=pondit', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = "SELECT * FROM `radio`";
    $result = $pdo->prepare($query);
    foreach ($result as $row) {
        $data[] = $row;
        echo "<pre>";
        print_r($row);
        echo "</pre>";
    }
} catch (PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}
?>
<html>
    <head>
        <title>Understanding CRUD BY Database using Textarea</title>
    </head>
    <body>


        <table border="1" width="" align="center" cellpadding="20">
            <div align="center">
                <a href="create.php">Create</a> 
            </div><br/>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td><?php echo $value['id'] ?></td>
                        <td>
                            <?php
                            if (array_key_exists('gender', $value)) {
                                echo $value['gender'];
                            } else {
                                echo "No Description available";
                            }
                            ?>
                        </td>
                        <td>
                            <a href="show.php?id=<?php echo $value['id']; ?>"> Show </a>  |
                            <a href="edit.php?id=<?php echo $value['id']; ?>"> Edit </a> |
                            <a href="delete.php?id=<?php echo $value['id']; ?>"> Delete</a> 
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="3" align="center">
                        <?php echo "No data available"; ?>
                    </td>
                </tr>
            <?php }
            ?>
        </table>    
    </body>
</html>