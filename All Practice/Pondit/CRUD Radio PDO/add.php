<?php

try {
    $pdo = new PDO('mysql:host=localhost;dbname=pondit', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = "INSERT INTO radio (gender) VALUES (:gender)";
    $stmt = $pdo->prepare($query);
    $stmt->execute(array(
        ':gender' => $_POST['gender']
    ));
    header('location:index.php');
} catch (PDOException $e) {
    echo 'Error: ' . $e->getMessage();
}