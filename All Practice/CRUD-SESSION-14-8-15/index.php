<?php
include_once 'src/applications.php';
$data = read(); // calling read function to the tempstorage.php file
//debug($data);
?>
<html>
    <head>
        <title>Understanding CRUD Using Session</title>
    </head>
    <body>
        <div align="center"><h2>Information table</h2>
            <a href="form.php"><b>Click Here to Add Date</b></a>
        </div><br/>
        <table border="1" width="950" align="center" cellpadding="15"> 
            <tr>
                <th>ID</th>  
                <th>User Name</th>
                <th>Full Name</th>
                <th>Password </th>
                <th>Email</th>
                <th>Date of Birth</th>
                <th>Action</th>
            </tr>
            <?php
            if (isset($data)) {
                $counter = 0;
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $counter++ ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('uname', $value)) {
                                echo $value['uname'];
                            } else {
                                echo "not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('fname', $value)) {
                                echo $value['fname'];
                            } else {
                                echo "not provide";
                            }
                            ?>
                        </td>
                         <td>
                            <?php
                            if (array_key_exists('pw', $value)) {
                                echo $value['pw'];
                            } else {
                                echo "not provide";
                            }
                            ?>
                        </td>
                         <td>
                            <?php
                            if (array_key_exists('email', $value)) {
                                echo $value['email'];
                            } else {
                                echo "not provide";
                            }
                            ?>
                        </td>
                         <td>
                            <?php
                            if (array_key_exists('date_of_birth', $value)) {
                                echo $value['date_of_birth'];
                            } else {
                                echo "not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <a href="show.php?id=<?php echo $counter ?>">  Details </a> |
                            <a href="edit.php?id=<?php echo $counter ?>">  Edit </a> |
                            <a href="delete.php?id=<?php echo $counter ?>">  Delete </a> 
                        </td>

                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="7" align="center">No available data</td>
                </tr>
            <?php }
            ?>

        </table>
    </body>
</html>