<?php

?>
<html>
    <head>
        <title>
            Understanding CRUD Using Session
        </title>
    </head>
    <body>
        <h2>Understanding CRUD Using Session</h2>
        <pre>
        <form action="src/process.php" method="POST">
            <fieldset>
                <label for="UserName">User Name</label>
                <input type="text" name="uname" id="UserName">
                <label for="fname">Full Name</label>
                <input type="text" name="fname" id="fname">
                <label for="pw">Password </label>
                <input type="password" name="pw" id="pw">
                <label for="email">Email</label>
                <input type="email" name="email" id="email">
                <label for="date_of_birth">Date of Birth</label>
                <input type="date" name="date_of_birth" id="date_of_birth">
                <input type="submit" value="Submit">
            </fieldset>
            
        </form>
        </pre>
    </body>
</html>