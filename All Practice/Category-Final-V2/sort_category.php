<?php
//include_once 'src/category/applications.php';
include_once("vendor/autoload.php");

use OroCoder\category\Category;
use OroCoder\category\Utility;

$sort_object = new Category;
//getting user id
$i = 1;
$user = $sort_object->users($i);

//Category sorting
$sort_value = $_POST['sort_by'];
if (isset($sort_value) && $sort_value == 'created_date') {
    $data = $sort_object->sortbydate($sort_value);
} else if (isset($sort_value) && !empty($sort_value)) {
    $data = $sort_object->sort($sort_value);
} else {
    header('location:manage_category.php');
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Checklist | Manage Category</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <link href="css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login body_color" >
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- START MENU OR HEADER OPTION -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="login.html">Checklist</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="header_menu">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">HOME <span class="sr-only">(current)</span></a></li>
                            <li><a href="guideline.html">GUIDELINE</a></li>
                            <li><a href="login.html">LOGOUT</a></li>
                            <li><a href="glossary.html">GLOSSARY</a></li>

                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- END HEADER -->
        <br/>
        <br/>
        <br/>
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="img/chayan.JPG" class="img-responsive" alt="">
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                CHAYAN ROY
                            </div>
                            <div class="profile-usertitle-job">
                                Web Developer & Designer
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR BUTTONS -->
                        <div class="profile-userbuttons">
                            <a href="#"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i></a>
                            <a href="#"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i></a>
                            <a href="#"><i id="social" class="fa fa-google-plus-square fa-3x social-gp"></i></a>
                            <a href="#"><i id="social" class="fa fa-envelope-square fa-3x social-em"></i></a>
                        </div>
                        <!-- END SIDEBAR BUTTONS -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="user_profile.html">
                                        <i class="fa fa-home fa-4x"></i>
                                        Dashboard </a>
                                </li>
                                <li>
                                    <a href="account_setting.html">
                                        <i class="fa fa-cog"></i>
                                        Account Settings </a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#myModal12">
                                        <i class="fa fa-pencil-square-o"></i>
                                        Add Project</a>
                                </li>
                                <li>
                                    <a href="manage_project.html">
                                        <i class="fa fa-list"></i>
                                        Manage Project</a>
                                </li>

                                <li>
                                    <a href="manage_category.php">
                                        <i class="fa fa-list"></i>
                                        Manage Category</a>
                                </li>
                                <li>
                                    <a href="manage_qc.html">
                                        <i class="fa fa-tags"></i>
                                        Manage QC</a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#myModal1">
                                        <i class="fa fa-spinner"></i>
                                        Suggest QC</a>
                                </li>

                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-content" >
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-lg-12  col-md-12  col-sm-10 col-sm-offset-1 col-xs-12">
                                <div>
                                    <form class="form-inline" action="src/category/cat_process.php" method="POST" >
                                        <input type="hidden" name="created_date" value="<?php echo date("Y-m-d"); ?>" class="col-md-4">
                                        <input type="hidden" name="created_by" value="<?php echo $user['username'] ?>">
                                        <div class="form-group">
                                            <label for="exampleInputName2"></label>
                                            <input type="text" class="form-control" name="cat_name" id="cat_name" placeholder="Enter Category Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail2"></label>
                                            <textarea class="form-control" rows="1" name="cat_desc" placeholder="Category Description" class=""></textarea>
                                            <!--<input type="text" class="form-control" name="cat_desc" id="cat_desc" placeholder="Ex: Desc about Category">-->
                                        </div>
                                        &nbsp;<button type="submit" class="btn btn-default">Add New Category</button>
                                        &nbsp;<a href="manage_category.php"><button type="submit" class="btn btn-default">View List</button></a>
                                    </form> 
                                </div>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col-lg-12  col-md-12  col-sm-10 col-sm-offset-1 col-xs-12">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <form action="search_category.php" method="POST">
                                        <div class="col-md-7" >
                                            <select class="form-control" name="search_option" id="sel1">
                                                <option value="">Category Search By</option>
                                                <option value="cat_name">Category Name</option>
                                                <option value="cat_desc">Category Description</option>
                                                <option value="created_by">Created By</option>
                                            </select>
                                        </div>
                                        <div class="input-group" class="col-md-5">
                                            <input type="text" name="search_value" class="form-control" placeholder="Search for...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">Go!</button>
                                            </span>
                                        </div> 
                                    </form>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">  
                                    <form action="sort_category.php" method="POST">
                                        <div class="col-md-6">
                                            <select class="form-control" name="sort_by" id="sel1">
                                                <option value=""> Sort by</option>
                                                <option value="cat_name">Category Name(A-Z)</option>
                                                <option value="created_date">Latest Create</option>
                                                <option value="modified_date">Last Modified</option>
                                            </select>
                                        </div>
                                        <div>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">Go!</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                        <hr>
                        <!--<hr>-->
                        <div class="row">
                            <div class="col-lg-10  col-md-10 col-md-offset-1 col-sm-12 ">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tr>
                                            <th> SL </th>
                                            <th>ID </th>
                                            <th>Category Name</th>
                                            <th >Actions</th>

                                        </tr>
                                        <?php
                                        if (isset($data) && !empty($data)) {
                                            $sl = 1;
                                            foreach ($data as $value) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $sl++ ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $value['id']; ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (array_key_exists('cat_name', $value)) {
                                                            echo $value['cat_name'];
                                                        } else {
                                                            echo "Not Provide";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <a href="show_category.php?id=<?php echo $value['id'] ?>">Details</a> |
                                                        <a href="edit_category.php?id=<?php echo $value['id'] ?>"> Edit</a> |
                                                        <a href="src/category/delete_category.php?id=<?php echo $value['id'] ?>"> Delete</a> 
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="3" align="center">No Category added yet</td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                                <div class="col-lg-6">
                                    <nav>
                                        <ul class="pagination">
                                            <li>
                                                <a href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li>
                                                <a href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="col-lg-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div> 

            </div>
        </div>
        <br/>
        <br/>

        <div class="footer">
            <footer class="copyright navbar navbar-default navbar-bottom">
                <span>2015 © CHECKLIST TEAM. ALL RIGHT RESERVE.</span>
            </footer>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Suggest QC</h4>
                    </div>
                    <div class="modal-body">
                        <label>Select Your QC Category</label>
                        <select class="form-control">
                            <option>body</option>
                            <option>footer</option>
                            <option>header</option>
                            <option>menu</option>
                            <option>content</option>
                        </select>
                        <br/>
                        <form class="form-horizontal">

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">YOUR QC</label>
                                <div class="col-sm-7">
                                    <input type="text" name="user_qc" class="form-control" id="inputEmail3" placeholder="enter your qc">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!--suggest QC javascript popover end here-->
    </body>
    <!-- END BODY -->
</html>