<?php

namespace OroCoder\category;

use OroCoder\category\Utility;

class Category {

    public $id = '';
    public $cat_name = '';
    public $cat_desc = '';
    public $created_date = '';
    public $modified_date = '';
    public $created_by = '';
    public $modified_by = '';
    public $conn = '';
    public $data = '';
    public $user = '';

    function __construct() {
        $this->conn = new \PDO('mysql:host=localhost;dbname=quickqc', 'root', '');
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function create($cat_name, $cat_desc, $created_date, $created_by) {
        try {
            $this->cat_name = $cat_name;
            $this->cat_desc = $cat_desc;
            $this->created_date = $created_date;
            $this->created_by = $created_by;

            $query = "INSERT INTO categories (cat_name,cat_desc,created_date,created_by) VALUES (:cat_name,:cat_desc,:created_date,:created_by)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':cat_name' => $this->cat_name,
                ':cat_desc' => $this->cat_desc,
                ':created_date' => $this->created_date,
                ':created_by' => $this->created_by,)
            );
            header('location:../../manage_category.php');
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function findall() {
        try {
            
            $query = "SELECT * FROM categories ORDER BY id ";
            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        return $this->data;
    }

    public function findone($id='') {
        try {
            $id =  mysql_real_escape_string($id);
            $query = "SELECT * FROM categories WHERE id=" . $id;

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function update($id, $cat_name, $cat_desc, $created_date, $created_by, $modified_date, $modified_by) {
//        $query = "UPDATE `quickqc`.`categories` SET `cat_name` = '" . $_POST ['cat_name'] . "', `cat_desc` = '" . $_POST ['cat_desc'] . "', `created_date` = '" . $_POST['created_date'] . "', `modified_date` = '" . $_POST['modified_date'] . "', `created_by` = '" . $_POST ['created_by'] . "', `modified_by` = '" . $_POST ['modified_by'] . "' WHERE `categories`.`id` =" . $id;
//        mysql_query($query);
//        header('location:../show_category.php?id=' .$id);

        $this->cat_name = $cat_name;
        $this->cat_desc = $cat_desc;
        $this->created_date = $created_date;
        $this->created_by = $created_by;
        $this->modified_date = $modified_date;
        $this->modified_by = $modified_by;
        $this->id = $id;

        $sql = "UPDATE `categories` SET `cat_name`=:cat_name,`cat_desc`=:cat_desc, `created_date` = :created_date, `created_by` = :created_by ,`created_by` = :created_by, `modified_date` = :modified_date, `modified_by` = :modified_by WHERE `id` = :id ";
        $stmt = $this->conn->prepare($sql);


        $stmt->bindParam(':cat_name', $cat_name, \PDO::PARAM_STR);
        $stmt->bindParam(':cat_desc', $cat_desc, \PDO::PARAM_STR);
        $stmt->bindParam(':created_date', $created_date, \PDO::PARAM_INT);
        $stmt->bindParam(':created_by', $created_by, \PDO::PARAM_STR);
        $stmt->bindParam(':modified_date', $modified_date, \PDO::PARAM_INT);
        $stmt->bindParam(':modified_by', $modified_by, \PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            header('location:../../show_category.php?id=' . $id);
        }
    }

    public function delete($id='') {
        try {
            $this->id = $id;
            $stmt = $this->conn->prepare('DELETE FROM categories WHERE id = :id');
            $stmt->execute(array('id' => $this->id));
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        header('location:../../manage_category.php');
    }

    public function users($i='') {
        try {
            $query = " SELECT * FROM `users` WHERE id=" . $i;

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->user = $row;
            }

            return $this->user;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function Search($option='', $value='') {
        try {
//            $value = mysql_real_escape_string($value);
            $q = "SELECT * FROM categories WHERE ".$option." LIKE '%" . $value . "%' ";
            $_result = $this->conn->query($q);
//            Utility:: debug($_result);
            foreach ($_result as $row) {
                $this->data[] = $row;
            }
            
            return $this->data;

//            Utility :: debug($this->data) ;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public function Searchall($value) {
        try {
            $q = "SELECT * FROM categories WHERE cat_name LIKE '%" . $value . "%' ";
            $_result = $this->conn->query($q);
            foreach ($_result as $row) {
                $this->data[] = $row;
            }           
            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    
    public function sort($sort_value){
        try {
            $q = "SELECT * FROM `categories` ORDER BY ".$sort_value." ASC";
            $_result = $this->conn->query($q);
            foreach ($_result as $row) {
                $this->data[] = $row;
            }           
            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public function sortbydate($sort_value){
        try {
            $q = "SELECT * FROM `categories` ORDER BY ".$sort_value." DESC";
            $_result = $this->conn->query($q);
            foreach ($_result as $row) {
                $this->data[] = $row;
            }           
            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    
}
