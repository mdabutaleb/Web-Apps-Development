<?php

//include_once 'applications.php';

include_once("../../vendor/autoload.php");

use OroCoder\category\Category;
use OroCoder\category\Utility;

$edit_object = new Category;
$data = $edit_object->findone($_POST['id']);

$id = $_POST['id'];
$cat_name = $_POST['cat_name'];
$cat_desc = $_POST['cat_desc'];
$created_date = $data['created_date'];
$created_by = $data['created_by'];
$modified_date = $_POST['modified_date'];
$modified_by = $_POST['modified_by'];

$edit_object->update($id, $cat_name, $cat_desc, $created_date, $created_by, $modified_date, $modified_by);
//echo "<pre>";
//var_dump($id, $cat_name, $cat_desc, $created_date, $created_by, $modified_date, $modified_by);
//echo "</pre>";

