<?php

//function for add data
function create() {
    if (strtoupper($_SERVER['REQUEST_METHOD'] == "POST")) {


        if (!array_key_exists('formdata', $_SESSION)) {
            $_SESSION['formdata'] = array();
        }
        $_SESSION['formdata'][] = $_POST;
        header('location:index.php');
    } else {
        header('location:form.php');
    }
}

//function for get one entry
function findone() {
    return $_SESSION['formdata'][$id];
}

// Function for get all data

function findall() {
    if (array_key_exists('formdata', $_SESSION)) {
        return $_SESSION['formdata'];
    }
}

//function for edit one item
function store() {
    if (strtoupper($_SERVER['REQUEST_METHOD'] == "POST")) {
        if (array_key_exists('formdata', $_SESSION)) {
            $_SESSION['formdata'][$_POST['id']] = $_POST;
        }
        header('location:../index.php');
    } else {
        header('location:../index.php');
    }
}

//function for delte one item
function delete() {
    if (strtoupper($_SERVER['REQUEST_METHOD'] == "GET")) {
        unset($_SESSION['formdata'][$_GET['id']]);
        header('location:../index.php');
    }
}
