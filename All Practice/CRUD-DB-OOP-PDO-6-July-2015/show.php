<?php
include_once 'lib/applications.php';
$Category1 = new Category();
$data = $Category1->findone();

?>
<html>
    <head>
        <title><?php echo TITLE ?></title>
    </head>

    <body>
        <div align="center">
            <a href="index.php">View All Data</a> | 
             <a href="form.php">Add More Data</a>
        </div><br/>
        <table border="1" cellpadding="15" align="center">
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Full Name</th>
                <th>Action </th>
            </tr>
            <?php
            if (isset($data) && !NULL) {
                    ?>
                    <tr>
                        <td><?php echo $data['id'] ?></td>
                        <td><?php
                            if (array_key_exists('user_name', $data)) {
                                echo $data['user_name'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td><?php
                            if (array_key_exists('pw', $data)) {
                                echo $data['pw'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td><?php
                            if (array_key_exists('email', $data)) {
                                echo $data['email'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('full_name', $data)) {
                                echo $data['full_name'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <a href="show.php?id=<?php echo $data['id'] ?>">View</a> | 
                            <a href="edit.php?id=<?php echo $data['id'] ?>"> Edit</a> |
                            <a href="delete.php?id=<?php echo $data['id'] ?>">Delete</a>
                        </td>
                    </tr>
                    <?php
                
            } else {
                ?>
                <tr>
                    <td colspan="5" align="center">No available Data</td>
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>