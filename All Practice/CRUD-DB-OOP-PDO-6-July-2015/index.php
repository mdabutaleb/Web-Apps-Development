<?php
//include_once 'lib/applications.php';
include_once 'lib/functions.php';
include_once 'vendor/autoload.php';


use Example\Crud\category;
$Category1 = new Category();
$data = $Category1->findall();

?>
<html>
    <head>
        <title><?php echo TITLE ?></title>
    </head>

    <body>
        <div align="center">
            <a href="form.php">Add More Data</a>
        </div><br/>
        <table border="1" cellpadding="15" align="center">
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Full Name</th>
                <th>Action </th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td><?php echo $value['id'] ?></td>
                        <td><?php
                            if (array_key_exists('user_name', $value)) {
                                echo $value['user_name'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td><?php
                            if (array_key_exists('pw', $value)) {
                                echo $value['pw'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td><?php
                            if (array_key_exists('email', $value)) {
                                echo $value['email'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td><?php
                            if (array_key_exists('full_name', $value)) {
                                echo $value['full_name'];
                            } else {
                                echo "Not provide";
                            }
                            ?>
                        </td>
                        <td>
                            <a href="show.php?id=<?php echo $value['id'] ?>">View</a> | 
                            <a href="edit.php?id=<?php echo $value['id'] ?>"> Edit</a> |
                            <a href="delete.php?id=<?php echo $value['id'] ?>">Delete</a>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="6" align="center">No available Data</td>
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>