<?php
include_once 'applications.php';
$data = findone($_GET['id']);
?>
<html>
    <head>
        <title> <?php ECHO TITLE; ?></title>
    </head>
    <body>
        <form action="edited.php" method="POST">
            <table width="700px" border="1" cellpadding="10" align="center">

                <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">

                <div align="center">
                    <a href="../index.php" align="center"> <h3>Back</h3> </a>
                </div>
                <tr>
                    <th> SL </th>
                    <th> User Name </th>
                    <th> Email </th>
                    <th> Date of Birth </th>
                    <th> Action </th>
                </tr>
                <?php
                if (isset($data) && !empty($data)) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $_GET['id'] + 1; ?>
                        </td>
                        <td>
                            <input type="text" name="uname" id="UserName" value="<?php
                            if (array_key_exists('uname', $data)) {
                                echo $data['uname'];
                            } else {
                                echo "Not provide";
                            }
                            ?>">

                        </td>
                        <td>
                            <input type="text" name="email" id="Email" value="<?php
                                   if (array_key_exists('email', $data)) {
                                       echo $data['email'];
                                   } else {
                                       echo "Not provide";
                                   }
                                   ?>">

                        </td>  
                        <td>
                            <input type="date" name="date_of_birth" id="Email" value="<?php
                            if (array_key_exists('date_of_birth', $data)) {
                                echo $data['date_of_birth'];
                            } else {
                                echo "Not provide";
                            }
                            ?>">

                        </td>
                        <td>
                            <input type="submit" value="Update"> 
                            <a href="delete.php?id=<?php echo $_GET['id']; ?>"> <input type="button" value="Delete"> </a>
                        </td>
                    </tr>
                    <?php
                } else {
                    header('location:../index.php');
                }
                ?>

            </table>
        </form>
    </body>
</html>