<?php
include_once 'lib/apps.php';
$Mycrud = new CRUD();
$data = $Mycrud->findall();
//debug($data);
?>
<html>
    <head>
        <title>CRUD BY DB</title>
    </head>
    <body>
        <div align="center">
                <a href="form.php">Add more data</a>
            </div><br/>
        <table border="1" align="center" cellpadding="15">
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            <?php
            if(isset($data) && !empty($data)){
            foreach ($data as $value) {
                ?>
                <tr>
                    <td><?php echo $value['id'] ?></td>
                    <td><?php
                        if (array_key_exists('user_name', $value))
                            echo $value['user_name'];
                        else {
                            echo "not provide";
                        }
                        ?>
                    </td>
                    <td><?php
                        if (array_key_exists('pw', $value))
                            echo $value['pw'];
                        else {
                            echo "not provide";
                        }
                        ?>
                    </td>
                     <td><?php
                        if (array_key_exists('email', $value))
                            echo $value['email'];
                        else {
                            echo "not provide";
                        }
                        ?>
                    </td>
                    <td>
                        <a href="show.php?id=<?php echo $value['id']?>">Full Info </a>| 
                        <a href="edit.php?id=<?php echo $value['id']?>"> Edit </a>| 
                        <a href="delete.php?id=<?php echo $value['id']?>"> Delete </a> 
                    </td>
                </tr>
            <?php } }
            else{ ?>
                <td colspan="5" align="center">No available data</td>
           <?php }?>
        </table>
    </body>
</html>