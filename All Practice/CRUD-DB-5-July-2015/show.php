<?php
include_once 'lib/apps.php';
$Mycrud = new CRUD();
$data = $Mycrud->findone();
//debug($data);
?>
<html>
    <head>
        <title>CRUD BY DB</title>
    </head>
    <body>
        <table border="1" align="center" cellpadding="15">
            <div align="center">
                <a href="index.php">List</a> | 
                <a href="form.php">Add</a>
            </div><br/>
            
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            <tr>
                <td><?php echo $data['id'] ?></td>
                <td><?php echo $data['user_name'] ?></td>
                <td><?php echo $data['pw'] ?></td>
                <td><?php echo $data['email'] ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $data['id']?>"> Edit </a>| 
                    <a href="delete.php?id=<?php echo $data['id']?>"> Delete </a>  
                </td>
            </tr>

        </table>
    </body>
</html>