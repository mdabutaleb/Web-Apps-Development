<?php
include_once("vendor/");

use App\Project;
use App\Report;
use App\Category;
use App\Sumon;

$project = new Project("My first Project");
echo "Project Title: " . $project->ptitle;

$report = new Report();
echo "<br/>Report Title: " . $report->rtitle;

$sumon = new Sumon("Sumon Mahmud");
echo "<br/>Sumon Title : " . $sumon->stitle;

$category = new category("SEO");
echo "<br/>Category Title : " . $category->ctitle;