<?php
include_once 'applications.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') { // anyone can't access directly this page
    if (array_key_exists('formdata', $_SESSION)) {
        $_SESSION['formdata'][$_POST['id']] = $_POST;
        header('location: index.php');
    }
} else {
    header('location: index.php');
}