<?php
include_once 'applications.php';
$data = '';
$data = stores();
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>CRUD</title>
        <link rel="stylesheet" href="css/style.css">

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="js/bootstrap.min.js" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div>
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">
                            Logo
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="index.php">All Data</a></li>
                            <li><a href="form.php">Signup</a></li>
                            <li><a href="signin.php">Sign in</a></li>
                        </ul>

                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <table class="table table-striped">
                        <tr>
                            <th>SL</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th colspan="3">Action</th>
                        </tr>
                        <?php
                        if (isset($data) && !empty($data)) {
                            foreach ($data as $key => $value) {
                                ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php
                                        if (array_key_exists('full_name', $value) && !empty($value['full_name'])) {

                                            echo $value['full_name'];
                                        } else {
                                            echo "Not provided";
                                        }
                                        ?>
                                    </td>
                                    <td><?php
                                        if (array_key_exists('email', $value) && !empty($value['email'])) {

                                            echo $value['email'];
                                        } else {
                                            echo "Not provided";
                                        }
                                        ?>
                                    </td>
                                    <td><?php
                                        if (array_key_exists('pw', $value) && !empty($value['pw'])) {

                                            echo $value['pw'];
                                        } else {
                                            echo "Not provided";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="show.php?id=<?php echo $key ?>">Full info | </a>
                                        <a href="edit.php?id=<?php echo $key ?>"> Edit | </a>
                                        <a href="delete.php?ids=<?php echo $key ?>"> Delete </a>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="5" align="center">
                                    <h3>No user information </h3>
                                    <a href="form.php"><h2>Add new user</h2></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>