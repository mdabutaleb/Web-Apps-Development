<?php
include_once 'applications.php';
if (strtoupper($_SERVER['REQUEST_METHOD']) == 'GET') {
    if (array_key_exists('ids', $_GET)) {
        if (isset($_GET['ids'])) {
            unset($_SESSION['formdata'][$_GET['ids']]);
            header('location:index.php');
        }
    }
}
