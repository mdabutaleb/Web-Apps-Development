<?php
function store(){
if (strtoupper($_SERVER['REQUEST_METHOD']) == "POST") {
    if (!array_key_exists('formdata', $_SESSION)) {
        $_SESSION['formdata'] = array();        
    }
    $_SESSION['formdata'][] = $_POST;
    header('location:index.php');
    
} 
else{
    header('location:form.php');
}
}

function findall(){
    if (array_key_exists('formdata', $_SESSION))
    return $_SESSION['formdata'];
}

function findone(){
    if(array_key_exists('formdata', $_SESSION))
    return $_SESSION['formdata'][$_GET['id']];  
}

function edit(){
    if (strtoupper($_SERVER['REQUEST_METHOD'] == "POST")) {
    if(array_key_exists('formdata', $_SESSION)){
        $_SESSION['formdata'][$_POST['id']] = $_POST;
    }
    header('location:../index.php');
    
}
}
