<?php
include_once 'lib/applications.php';
$data = findall();
//debug($data);
?>
<a href="form.php"> Add New Data</a>
<table border="1" cellpadding="10" align="center">
    <tr>
        <th>SL </th>
        <th>User Name</th>
        <th>Email</th>
        <th>Date of birth</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($data) && !empty($data)) {
        foreach ($data as $key => $value) {
            ?>
            <tr>

                <td><?php echo $key + 1 ?></td>
                <td><?php
                    if (array_key_exists('uname', $value)) {
                        echo $value['uname'];
                    } else {
                        echo "Not Provide";
                    }
                    ?>
                </td>
                <td><?php
                    if (array_key_exists('email', $value)) {
                        echo $value['email'];
                    } else {
                        echo "Not Provide";
                    }
                    ?>
                </td>
                <td><?php
                    if (array_key_exists('date_of_birth', $value)) {
                        echo $value['date_of_birth'];
                    } else {
                        echo "Not Provide";
                    }
                    ?>
                </td>
                <td>
                    <a href="show.php?id=<?php echo $key ?>">View</a> | 
                    <a href="edit.php?id=<?php echo $key ?>">Edit</a> | 
                    <a href="delete.php?id=<?php echo $key ?>">Delete</a>
                </td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="5" align="center"> No Available Data <a href="form.php"> Add New Data</a></td>
        </tr>
        <?php
    }
    ?>

</table>