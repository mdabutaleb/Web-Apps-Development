-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2015 at 12:03 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mycrud`
--

-- --------------------------------------------------------

--
-- Table structure for table `cruddb1`
--

CREATE TABLE IF NOT EXISTS `cruddb1` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `class` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cruddb1`
--

INSERT INTO `cruddb1` (`id`, `name`, `class`) VALUES
(2, 'Sumon', 'ten');

-- --------------------------------------------------------

--
-- Table structure for table `crudone`
--

CREATE TABLE IF NOT EXISTS `crudone` (
  `id` int(11) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `pw` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crudone`
--

INSERT INTO `crudone` (`id`, `user_name`, `pw`, `email`) VALUES
(8, 'sumonMahmud', '25121990', 'sumon.mahmud@innocent.com.com');

-- --------------------------------------------------------

--
-- Table structure for table `crudthree`
--

CREATE TABLE IF NOT EXISTS `crudthree` (
  `id` int(11) NOT NULL,
  `full_name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `money` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crudthree`
--

INSERT INTO `crudthree` (`id`, `full_name`, `email`, `city`, `money`) VALUES
(5, 'Sumon Mahmud', 'sumon.pdk.bd@gmail.com', 'Dhaka', 543543),
(8, 'Sumon Mahmud', 'sumon.pdk.bd@gmail.com', 'Kushtia', 2000),
(10, 'Rony', 'rony@gmail.com', 'naraongonge', 120000),
(11, 'tony', 'tony@gmail.com', 'dhaka', 230),
(12, 'Bony', 'bony@gmail.com', 'Dhaka', 20),
(13, 'Mukta', 'mukta@gmail.com', 'Jessore', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `crudtwo`
--

CREATE TABLE IF NOT EXISTS `crudtwo` (
  `id` int(11) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `pw` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `full_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crudtwo`
--

INSERT INTO `crudtwo` (`id`, `user_name`, `pw`, `email`, `full_name`) VALUES
(19, 'google', '22', 'mukta.jes.bd@gmail.com', 'Rafats'),
(30, 'Sumo', '25', 'sometingfd@g.comk', 'Rony'),
(31, 'Romana', '23', 'roma@na.com', 'Rakib');

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `location` varchar(20) NOT NULL,
  `full_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `name`, `location`, `full_name`) VALUES
(1, 'Rafats', 'Dhaka', 'testd'),
(2, 'Rakib', 'Noakhali', ''),
(3, 'Sakil', 'Dhaka', ''),
(4, 'Mijan', 'Dhaka', ''),
(5, 'Aksh', 'Feni', '');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `uname`, `email`, `date_of_birth`) VALUES
(3, 'test', 'test@ok.com', '2015-07-14'),
(4, 'dfd', 'mukta.jes.bd@gmail.coms', '2015-07-07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cruddb1`
--
ALTER TABLE `cruddb1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crudone`
--
ALTER TABLE `crudone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crudthree`
--
ALTER TABLE `crudthree`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crudtwo`
--
ALTER TABLE `crudtwo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crudone`
--
ALTER TABLE `crudone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `crudthree`
--
ALTER TABLE `crudthree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `crudtwo`
--
ALTER TABLE `crudtwo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
