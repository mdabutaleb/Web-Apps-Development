<?php

namespace Example\Crud;

class User {

    public $id = '';
    public $full_name = '';
    public $email = '';
    public $city = '';
    public $data = array();
    public $conn = '';

    function __construct() {

        $this->conn = new \PDO('mysql:host=localhost;dbname=mycrud', 'root', '');
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function create() {
        try {
            $this->full_name = $_POST['full_name'];
            $this->email = $_POST['email'];
            $this->city = $_POST['city'];
            $query = "INSERT INTO crudthree (full_name,email,city) VALUES (:full_name,:email,:city)";
            $stmt = $this->conn->prepare($query);

            $stmt->execute(array(
                ':full_name' => $this->full_name,
                ':email' => $this->email,
                ':city' => $this->city)
            );
            echo $stmt->rowCount(); // 1
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            header('location:index.php');
        }
    }

    public function findall() {

        try {
            $query = "SELECT * FROM crudthree";
            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        return $this->data;
    }

    public function findone() {
        try {
            $query = "SELECT * FROM crudthree WHERE id=" . $_GET['id'];

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function edit($id,$full_name,$email,$city) {

//        $this->id =$id;
//        $this->full_name =$full_name;
//        $this->email =$email;
//        $this->city =$city;

        $sql = "UPDATE `crudthree` SET `full_name` = :full_name, `email` = :email, `city` = :city WHERE `id` = :id ";

        $stmt = $this->conn->prepare($sql);


        $stmt->bindParam(':full_name', $full_name, \PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, \PDO::PARAM_STR);

        $stmt->bindParam(':city', $city, \PDO::PARAM_INT);

        $stmt->bindParam(':city', $city, \PDO::PARAM_STR);

        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            header("location: index.php");
        }
    }

    public function delete() {
        try {
            $this->id = $_GET['id'];
            $stmt = $this->conn->prepare('DELETE FROM crudthree WHERE id = :id');
            $stmt->execute(array('id' => $this->id));
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        header('location:index.php');
    }
    public function city() {
        try {
            $query = "SELECT Distinct location FROM info";
            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

}
