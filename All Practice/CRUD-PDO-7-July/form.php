<?php
//include_once 'lib/application.php';
include_once 'vendor/autoload.php';
use Example\Crud\User;

$city = new User();
$city = $city->city();
//print_r($city);

?>

<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
    </head>
    <body>
        <div align="center">
            <a href="index.php">View All Data</a>
        </div><br/>
        <form action="process.php" method="POST" align="center">
            <table width="700px" align="center">

                <label> Full Name </label>
                <input type="text" name="full_name" id="Full_name">
                <label> Email </label>
                <input type="email" name="email" id="Email">

                <label> City </label>
                <select name="city"> 
                    <option value="">Select City</option>
                   <?php
                    if (isset($city)) {
                        foreach ($city as $loc) {
                            ?>
                            <option value="<?php echo $loc['location'] ?>"><?php echo$loc['location'] ?></option>
                            <?php
                        }
                    }
                    ?>
                </select><br/>
                <input type="submit" value="Submit">
            </table>
        </form>
    </body>
</html>
