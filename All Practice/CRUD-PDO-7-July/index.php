<?php
//include_once 'lib/application.php';
include_once 'vendor/autoload.php';


use Example\Crud\User;

$user = new User();
$data = $user->findall();

?>

<html>
    <head>
        <title>

        </title>
    </head>
    <body>
        <div align="center">
            <a href="form.php"> Add Another Data</a>
        </div><br/>
        <table align="center" cellpadding="15" border="1">
            <tr>
                <th>ID</th>
                <th>Full Name </th>
                <th>Email </th>
                <th>City</th>
                <th>Action</th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $value) {
                    ?>
                    <tr>
                        <td><?php echo $value['id'] ?></td>
                        <td>
                            <?php
                            if (array_key_exists('full_name', $value)) {
                                echo $value['full_name'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('email', $value)) {
                                echo $value['email'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (array_key_exists('city', $value)) {
                                echo $value['city'];
                            } else {
                                echo "Not Provide";
                            }
                            ?>
                        </td>
                        <td>
                            <a href="show.php?id=<?php echo $value['id']?>">View</a> | 
                            <a href="edit.php?id=<?php echo $value['id']?>"> Edit </a> |
                            <a href="delete.php?id=<?php echo $value['id']?>"> Delete</a>
                        </td>

                    </tr>
                <?php }
            }
            ?>
        </table>
    </body>
</html>
