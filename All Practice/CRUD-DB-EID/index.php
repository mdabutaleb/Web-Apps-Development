<?php
include_once 'lib/applications.php';

$object = new Crudfour;
$data = $object->findall();
?>
<a href="form.php">Add Data</a>
<table border="1" cellpadding="20" align="center">
    <tr>
        <th>ID</th>
        <th>User Name</th>
        <th>Email </th>
        <th>Date of Birth</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($data) && !empty($data)) {
        foreach ($data as $value) {
            ?>
            <tr>
                <td><?php echo $value['id'] ?></td>
                <td><?php echo $value['uname'] ?></td>
                <td><?php echo $value['email'] ?></td>
                <td><?php echo $value['date_of_birth'] ?></td>
                <td>
                    <a href="show.php?id=<?php echo $value['id']?>">Show</a> | 
                    <a href="edit.php?id=<?php echo $value['id']?>">Edit</a> | 
                    <a href="delete.php?id=<?php echo $value['id']?>">Delete</a> 
                </td>
            </tr>
        <?php
        }
    } else {
        
    }
    ?>
</table>
