<?php
include_once 'lib/apps.php';
$data = findall();
//debug($data);
?>
<html>
    <head>
        <title><?php echo TITLE; ?></title>
    </head>
    <body>
        <a href="form.php">Add another data</a>
        <table align="center" width="800px" border="1">
            <tr>
                <th>SL</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Action </th>
            </tr>
            <?php
            if (isset($data) && !empty($data)) {
                foreach ($data as $key=> $value){
                ?>
                <tr>
                    <td>
                        <?php echo $key+1 ?>
                    </td>
                    <td>
                       <?php 
                      if(array_key_exists('uname', $value)) {
                          echo $value['uname'];
                      }
                      else{
                          echo "Not provide";
                      }
                       ?>
                    </td>
                    <td>
                       <?php 
                      if(array_key_exists('email', $value)) {
                          echo $value['email'];
                      }
                      else{
                          echo "Not provide";
                      }
                       ?>
                    </td>
                    <td>
                        <a href="src/show.php?id=<?php echo $key ?>">Show Info </a>
                        <a href="src/edit.php?id=<?php echo $key ?>">| Edit </a>
                        <a href="src/delete.php?id=<?php echo $key ?>">| Delete</a>
                    </td>
                </tr>
            <?php }} 
            else{
                ?>
                <td colspan="4" align="center">No available data</td>
           <?php } ?>
        </table>
    </body>
</html>