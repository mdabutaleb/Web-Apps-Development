
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>CRUD</title>
        <!--<link rel="stylesheet" href="css/style.css">-->

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="js/bootstrap.min.js" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div>
            <nav class="navbar navbar-default">
                <div class="container">
                    <!--Brand and toggle get grouped for better mobile display--> 
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">
                            Logo
                        </a>
                    </div>

                    <!--Collect the nav links, forms, and other content for toggling--> 
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="#">Logout</a></li>
                        </ul>

                    </div> 
                </div> 
            </nav>
        </div>
        <div class="jumbotron container-fluid">
            <div class="container"><br/>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 ">
                        <div class="row">
                            <div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-12">
                                <a href="index.php"><button type="button" class="btn btn-default"> Dashboard</button></a>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="category.php"><button type="button" class="btn btn-primary"> Category List</button></a>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="add-category.php"> <button type="button" class="btn btn-primary">Add Category</button></a>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="item-list.php">  <button type="button" class="btn btn-primary">All Item List</button></a>
                            </div> 
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="add-item.php"><button type="button" class="btn btn-primary">Add New Item</button></a>
                            </div> 
                        </div><br/>
                    </div>

                </div>
            </div><br/><hr><br><br>
            <div class="container">
                <div class="row">
                    <form class="form-inline col-md-6 col-md-offset-3">
                        <div class="form-group">
                            <label for="exampleInputName2">New Category Name</label>
                            <input type="text" class="form-control" id="exampleInputName2" placeholder="Enter Category Name">
                        </div>
                        <button type="submit" class="btn btn-primary">Add Category</button>
                    </form>
                </div>

            </div><br><br><br/><br/><br><br/><br/><br><br/><br/><br><br/><br/>

        </div>
        <div align="center">
            <p> Copyright &copy; ORO Coder</p>
        </div>

        <!--        <div class="container">
                    <div class="row">
        
                    </div>
                </div>-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>