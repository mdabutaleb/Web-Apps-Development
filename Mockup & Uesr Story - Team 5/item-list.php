
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>CRUD</title>
        <!--<link rel="stylesheet" href="css/style.css">-->

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="js/bootstrap.min.js" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div>
            <nav class="navbar navbar-default">
                <div class="container">
                    <!--Brand and toggle get grouped for better mobile display--> 
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">
                            Logo
                        </a>
                    </div>

                    <!--Collect the nav links, forms, and other content for toggling--> 
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="#">Logout</a></li>
                        </ul>

                    </div> 
                </div> 
            </nav>
        </div>
        <div class="jumbotron container-fluid">
            <div class="container"><br/>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 ">
                        <div class="row">
                            <div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-12">
                                <a href="index.php"><button type="button" class="btn btn-default"> Dashboard</button></a>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="category.php"><button type="button" class="btn btn-primary"> Category List</button></a>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="add-category.php"> <button type="button" class="btn btn-primary">Add Category</button></a>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="item-list.php">  <button type="button" class="btn btn-primary">All Item List</button></a>
                            </div> 
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="add-item.php"><button type="button" class="btn btn-primary">Add New Item</button></a>
                            </div> 
                        </div><br/>
                    </div>

                </div>
            </div><br/><hr>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 ">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr align="center">
                                    <th>SL</th>
                                    <th>Item Name</th>
                                    <th>Category </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Font familiy</td>
                                    <td>Font</td>
                                    <td><a href="#" class="btn btn-info">Update</a>
                                        <a href="#" class="btn btn-danger">Delete</a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Font Size</td>
                                    <td>Font</td>
                                    <td><a href="#" class="btn btn-info">Update</a>
                                        <a href="#" class="btn btn-danger">Delete</a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Font Color</td>
                                    <td>Font</td>
                                    <td><a href="#" class="btn btn-info">Update</a>
                                        <a href="#" class="btn btn-danger">Delete</a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Font height</td>
                                    <td>Font</td>
                                    <td><a href="#" class="btn btn-info">Update</a>
                                        <a href="#" class="btn btn-danger">Delete</a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Font width</td>
                                    <td>Font</td>
                                    <td><a href="#" class="btn btn-info">Update</a>
                                        <a href="#" class="btn btn-danger">Delete</a> 
                                    </td>
                                </tr>

                            </tbody>
                        </table> 
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <nav class="col-md-8 col-md-offset-2">
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <br/><br/>

        </div>
        <div align="center">
            <p> Copyright &copy; ORO Coder</p>
        </div>



        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>