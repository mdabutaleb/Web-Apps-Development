<?php
class person {
    public $eye = 2;
    public $hand = 2;
    public $leg = 2;
    public $weight;
    public $height;
    public $name;
    public function common(){
        echo $this->eye;
        echo $this->hand;
        echo $this->weight;
        echo $this->name;
    }
    public function eat() {
        echo $this->name . " Is eating & weight is <br/>";
        echo $this->weight;
    }
    public function walk() {
        echo $this->name . " is walking <br/>";
    }
    public function shop() {
        echo $this->name . " is on shopping <br/>";
    }

}
//subclass
class subclass extends person {
    public $age;
    public $name = "Rony";
}
// More Subclass 
class moresub extends subclass {
    public $name = "<br/>Sumon";
    function __construct() {
        $this->name = "<br/>Sumon Mahmud";
    }
}