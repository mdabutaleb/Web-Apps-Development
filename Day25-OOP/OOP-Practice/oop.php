<?php
include_once 'class.php';
echo "<br/> Output of Man1<hr> ";

$man1 = new person();
$man1->weight = 20;
$man1->height = 5;
$man1->name = "Sumon Mahmud";
$man1->eat();

echo "<br/><br/> Output of Man2 ";
$man2 = new person();
$man2->weight = 100;
$man2->height = 500;
$man2->name = 'Mukta';
echo "<br/>";
$man2->eat();
$man2->shop();
$man2->walk();
echo "<hr>";
$subobject = new subclass();
$subobject->shop();
echo $subobject->name;

$moresubobj = new moresub();
$moresubobj->shop();
$moresubobj->common();